from django import forms

from blog.template.blog.models import perrito

class formServicio(forms.ModelForm):
    class Meta:
        model = perrito
        fields = [
            'nombredeperro',
            'raza',
            'descripcion',
            'imagen',
            'estadoperro',
        ]
