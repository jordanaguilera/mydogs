from django.shortcuts import render

def index(request):
    return render(request, 'blog/index.html', {})
def registro(request):
    return render(request, 'blog/registro.html', {})
def galeria(request):
    return render(request, 'blog/galeria.html', {})
