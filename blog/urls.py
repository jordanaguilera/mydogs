from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('blog/registro/', views.registro, name='registro'),
    path('blog/galeria/', views.galeria, name='galeria'),
]